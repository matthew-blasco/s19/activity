var log = console.log;

// ACTIVITY 
/* Exponent Operator */

//3. && 4.
const getCube = (a,b) => `The cube of ${a} is ` + (Math.pow(a,b));

log(getCube(2,3));

//5.
const address = [`258 Washington Ave NW,`,`California`,`90011`]

//6.
const [street, state, zip] = address;

const message = `I live at ${street} ${state} ${zip}`;

log(message);

//7.
const animal = {
  name: 'Lolong',
  specimen: 'saltwater crocodile',
  weight: '1075 kgs',
  size: `20 ft 3 in`, 
}
myAnimal(animal);

//8. STATUS: OK
function myAnimal({name, specimen, weight, size}){
  const message2 = `${name} was a ${specimen}. He weighed at ${weight} with a measurement of ${size}.`
  log(message2);
}

//9. STATUS: OK
var numbers = [1,2,3,4,5];

//10. STATUS: OK
let printN = numbers.forEach(number => log(number));

//11. STATUS: OK = 15
let reduceNumber = numbers.reduce((tNumber, num)  => tNumber + num);

log(reduceNumber);


    //11.1 Traditional method Reference** STATUS: OK = 15 
    /*function getSum(total, num) {
        return total + Math.round(num);
      }
        log(numbers.reduce(getSum, 0));*/

//12.

class Dog{
  constructor(name, age, breed){
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

let dog1 = new Dog("Frankie", "5", "Miniature Dachshund");
log(dog1);